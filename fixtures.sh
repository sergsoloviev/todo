#!/usr/bin/env bash

APP="todo"
DATA="data0"
python manage.py dumpdata -e contenttypes -e auth.Permission -e sessions -e admin.logentry --indent 4 --natural-foreign > $APP/fixtures/$DATA.json