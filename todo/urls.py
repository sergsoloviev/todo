#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url
from todo.views import home, project, issue


urlpatterns = [
    url(r'^$',
        home.Home.as_view(),
        {'template': 'todo/home.html'},
        name='home'
        ),
    url(r'^project/(?P<project_id>[0-9]+)/$',
        project.Project.as_view(),
        {'template': 'todo/project.html'},
        name='project'
        ),
    url(r'^project/create/$',
        project.ProjectCreate.as_view(),
        {'template': 'todo/project_create.html'},
        name='project_create'
        ),
    url(r'^project/(?P<project_id>[0-9]+)/issue/create/$',
        issue.IssueCreate.as_view(),
        {'template': 'todo/issue_create.html'},
        name='issue_create'
        ),
]
