#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pprint import pprint
import logging

from django.contrib.auth.decorators import login_required
# from django.utils import translation

from todo.models.project import Project


logger = logging.getLogger('core')


def view_decorator(function):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            # language = translation.get_language_from_request(request)
            # language = manager.language
            # translation.activate(language)
            # request.LANGUAGE_CODE = translation.get_language()
            projects = Project.objects.filter(users__in=[request.user])
            kwargs['projects'] = projects
            data = view_func(request, *args, **kwargs)
            log = {
                'request': {
                    'id': request.META.get('HTTP_X_REQUEST_ID'),
                    'method': request.method,
                    'path': request.path,
                    'body': request.body,
                    # 'headers': request.headers,
                },
                'response': {
                    'status_code': data.status_code,
                }
            }
            logger.debug(log)
            return data
        return _view
    return _dec(function)


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view_decorator(view))
