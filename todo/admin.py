#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from todo.models.project import Project
from todo.models.status import Status
from todo.models.issue import Issue
from todo.models.history import History


class ProjectAdmin(admin.ModelAdmin):
    fields = [
        'name',
        'project_id',
        'users'
    ]
    list_display = [
        'id',
        'name',
        'project_id',
    ]
    ordering = ('id', 'name')

admin.site.register(Project, ProjectAdmin)


class StatusAdmin(admin.ModelAdmin):
    fields = [
        'name',
    ]
    list_display = [
        'id',
        'name',
    ]
    ordering = ('id', 'name')

admin.site.register(Status, StatusAdmin)


class IssueAdmin(admin.ModelAdmin):
    fields = [
        'project',
        'name',
        'description',
        'resolution',
        'reporter',
        'assignee',
        'touched',
        # 'created',
        # 'modified',
        'status',
    ]
    list_display = [
        'id',
        'project',
        'name',
        'description',
        'resolution',
        'reporter',
        'assignee',
        'touched',
        'created',
        'modified',
        'status',
    ]
    ordering = ('id', '-modified')

admin.site.register(Issue, IssueAdmin)


class HistoryAdmin(admin.ModelAdmin):
    fields = [
        'issue',
        # 'date',
        'user',
        'status0',
        'status1',
    ]
    list_display = [
        'id',
        'issue',
        'date',
        'user',
        'status0',
        'status1',
    ]
    ordering = ('-date', 'issue')

admin.site.register(History, HistoryAdmin)
