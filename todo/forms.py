#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pprint import pprint

from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User

from todo.models.project import Project
from todo.models.issue import Issue
from todo.models.status import Status


class ProjectCreateForm(ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    project_id = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all()
    )

    class Meta:
        model = Project
        fields = ['name', 'project_id', 'users']


class IssueCreateForm(ModelForm):
    project = forms.ModelChoiceField(queryset=Project.objects.all())
    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    description = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'form-control'})
    )
    reporter = forms.ModelChoiceField(queryset=User.objects.all())
    assignee = forms.ModelChoiceField(queryset=User.objects.all())
    touched = forms.ModelChoiceField(queryset=User.objects.all())
    status = forms.ModelChoiceField(queryset=Status.objects.all())

    class Meta:
        model = Issue
        fields = [
            'project',
            'name',
            'description',
            # 'resolution',
            'reporter',
            'assignee',
            'touched',
            # 'created',
            # 'modified',
            'status',
        ]
