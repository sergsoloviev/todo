#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from todo.models.status import Status
from todo.models.project import Project


class Issue(models.Model):
    project = models.ForeignKey(Project)
    name = models.CharField(max_length=250)
    description = models.TextField(default=None, null=True)
    resolution = models.TextField(default=None, null=True, blank=True)
    reporter = models.ForeignKey(User, related_name='reporter')
    assignee = models.ForeignKey(User, related_name='assignee')
    touched = models.ForeignKey(User, related_name='touched')
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified = models.DateTimeField(auto_now=True, blank=True, null=True)
    status = models.ForeignKey(Status)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
