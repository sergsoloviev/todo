#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from todo.models.status import Status
from todo.models.issue import Issue


class History(models.Model):
    issue = models.ForeignKey(Issue)
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    user = models.ForeignKey(User)
    status0 = models.ForeignKey(Status, related_name='past')
    status1 = models.ForeignKey(Status, related_name='current')

    def __unicode__(self):
        return "%s - %s - %s - %s", \
               (self.date, self.user, self.status0, self.status1)

    def __str__(self):
        return "%s - %s - %s - %s", \
               (self.date, self.user, self.status0, self.status1)
