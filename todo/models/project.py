#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    name = models.CharField(max_length=100)
    project_id = models.CharField(max_length=50)
    users = models.ManyToManyField(User, default=None)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return "%s - %s" % (self.name, self.project_id,)
