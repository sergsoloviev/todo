#!/usr/bin/env python
# -*- coding: utf-8 -*-
# from pprint import pprint
from django.shortcuts import render
from django.views.generic import View

from todo.decorators import LoginRequiredMixin


class Home(LoginRequiredMixin, View):

    def get(self, request, template, projects):
        context = {
            'ok': 200,
            'project': None,
            'projects': projects,
        }
        return render(request, template, context)
