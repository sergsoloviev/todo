#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pprint import pprint
from django.shortcuts import render, redirect
from django.views.generic import View

from todo.decorators import LoginRequiredMixin
from todo.forms import IssueCreateForm


class IssueCreate(LoginRequiredMixin, View):

    def get(self, request, template, projects, project_id):
        project = projects.get(pk=int(project_id))
        form = IssueCreateForm(initial={
            'reporter': request.user,
            'assignee': request.user,
            'touched': request.user,
            'project': project,
            'status': 1
        })
        form.fields['project'].queryset = projects
        # form.fields['project']
        context = {
            'form': form,
            'project': project,
            'projects': projects,
        }
        return render(request, template, context)

    def post(self, request, template, projects, project_id):
        project = projects.get(pk=int(project_id))
        form = IssueCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/project/%d/" % int(project_id))
        else:
            pprint(form.errors)
            context = {
                'form': form,
                'project': project,
                'projects': projects,
            }
            return render(request, template, context)

