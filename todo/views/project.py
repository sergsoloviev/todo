#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pprint import pprint
from django.shortcuts import render, redirect
from django.views.generic import View

from todo.decorators import LoginRequiredMixin
from todo.models.issue import Issue
from todo.forms import ProjectCreateForm


class Project(LoginRequiredMixin, View):

    def get(self, request, template, projects, project_id):
        project = projects.get(pk=int(project_id))
        issues = Issue.objects.filter(project=project)
        context = {
            'ok': 200,
            'project': project,
            'projects': projects,
            'issues': issues,
        }
        return render(request, template, context)


class ProjectCreate(LoginRequiredMixin, View):

    def get(self, request, template, projects):
        form = ProjectCreateForm()
        context = {
            'form': form,
            'projects': projects,
        }
        return render(request, template, context)

    def post(self, request, template, projects):
        form = ProjectCreateForm(request.POST)
        if form.is_valid():
            obj = form.save()
            # if no self user: add
            self_user = obj.users.filter(id=request.user.id).first()
            if self_user is None:
                obj.users.add(request.user)
            return redirect("/project/%d/" % obj.id)
        else:
            context = {
                'form': form,
                'projects': projects,
            }
            return render(request, template, context)

