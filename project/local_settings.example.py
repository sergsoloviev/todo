"""
Django settings for project project.

Generated by 'django-admin startproject' using Django 1.9.2.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/
`
For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os
from sys import argv

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

SITE_ID = 1

SESSION_COOKIE_AGE = 1800

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'channels',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# AUTHENTICATION_BACKENDS = [
    # 'django.contrib.auth.backends.RemoteUserBackend',
# ]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR + '/templates/'
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
IS_TEST = False
if argv and 1 < len(argv):
    IS_TEST = 'test' == argv[1]
else:
    IS_TEST = False

if IS_TEST:
    # DB_ENGINE = 'django.db.backends.sqlite3'
    # DB_NAME = os.path.join(BASE_DIR, 'db.sqlite3')
    # DB_NAME = ':memory:'
    DB_ENGINE = 'django.db.backends.postgresql_psycopg2'
    DB_NAME = 'todo'
else:
    # DB_ENGINE = 'django.db.backends.mysql'
    DB_ENGINE = 'django.db.backends.postgresql_psycopg2'
    DB_NAME = 'todo'

DATABASES = {
    'default': {
        'HOST':             'postgres_ip',
        'ENGINE':           DB_ENGINE,
        'NAME':             DB_NAME,
        'USER':             'postgres_user',
        'PASSWORD':         'postgres_password',
    }
}

# Channels
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'asgi_redis.RedisChannelLayer',
        'CONFIG': {
            'hosts': [('192.168.70.70', 6379)],
            # "hosts": [
            #     ("192.168.70.70", 6379),
            #     ("192.168.70.71", 6379)
            # ],
        },
        'ROUTING': 'project.routing.channel_routing',
    },
}

# Logs
LOG_DIR = BASE_DIR + '/log/'
# LOG_FILE = datetime.utcnow().strftime("%d.%m.%Y") + '.log'
LOG_FILE = 'site.log'
CORE_LOG_FILE = 'core.log'
TASK_LOG_FILE = 'task.log'

# Logging
# class UtcFormatter(logging.Formatter):
#     converter = time.gmtime

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        # 'utc': {
        #     '()': 'UtcFormatter',
        #     'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
        #     'datefmt': "%d/%b/%Y %H:%M:%S"
        # },
        'verbose': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'log/' + LOG_FILE,
            'formatter': 'verbose'
        },
        'core': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'log/' + CORE_LOG_FILE,
            'formatter': 'verbose'
        },
        'task': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'log/' + TASK_LOG_FILE,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': False,
            'level':'DEBUG',
        },
        'core': {'handlers': ['core'], 'level': 'DEBUG'},
        'task': {'handlers': ['task'], 'level': 'DEBUG'},
    }
}

TABLES_PER_PAGE = 30

# APPEND_SLASH = True

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'
# LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT= BASE_DIR + STATIC_URL

MEDIA_URL = '/media/'
MEDIA_ROOT= BASE_DIR + MEDIA_URL

LOGIN_REDIRECT_URL = '/'

